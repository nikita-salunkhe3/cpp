//call by value

#include<iostream>
void fun(int x,int y){

	int temp=x;
	x=y;
	y=temp;

	std::cout<<x<<std::endl;//20
	std::cout<<y<<std::endl;//10
}
int main(){

	int x=10;
	int y=20;

	std::cout<<x<<std::endl;//10
	std::cout<<y<<std::endl;//20

	fun(x,y);

	std::cout<<x<<std::endl;//10
	std::cout<<y<<std::endl;//20
}

