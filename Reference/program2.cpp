#include<iostream>

int main(){
	int x=10;

	int &y=x;

	int *ptr=&x;

	std::cout<<x<<std::endl;//10
	std::cout<<y<<std::endl;//10
	std::cout<<ptr<<std::endl;//0X100

	std::cout<<&x<<std::endl;//0X100
	std::cout<<&y<<std::endl;//0X100
	std::cout<<&ptr<<std::endl;//0X200

	std::cout<<sizeof(x)<<std::endl;//4
	std::cout<<sizeof(y)<<std::endl;//4
	std::cout<<sizeof(ptr)<<std::endl;//8
}

