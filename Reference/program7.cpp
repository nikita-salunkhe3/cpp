//call by reference

#include<iostream>
void fun(int &ptr1,int &ptr2){

	int temp=ptr1;
	ptr1=ptr2;
	ptr2=temp;

	std::cout<< ptr1 <<std::endl;//20
	std::cout<< ptr2 <<std::endl;//10
}
int main(){
	int x=10;
	int y=20;

	std::cout<< x <<std::endl;//10
	std::cout<< y <<std::endl;//20

	fun(x,y);

	std::cout<< x <<std::endl;//20
	std::cout<< y <<std::endl;//10
	return 0;
}
