#include<iostream>
void add(int x,int y){
	return x+y;
}

void add(int x,float y){
	return x+y;
}

int main(){
	std::cout<< add(10.11,10.11) <<std::endl;
}
/*
 * output:
 *  error: call of overloaded ‘add(double, double)’ is ambiguous
   12 |  std::cout<< add(10.11,10.11) <<std::endl;
      |                             ^
program4.cpp:3:5: note: candidate: ‘int add(int, int)’
    3 | int add(int x,int y){
      |     ^~~
program4.cpp:7:5: note: candidate: ‘int add(int, float)’
    7 | int add(int x,float y){
      |     ^~~
*/
