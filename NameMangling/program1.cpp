/*NAME MANGLING
 *jase JAVA madhe Overloading chi concept ahe tashich CPP madhe sudha Overloading chi concept
  ahe pn farak ekch ahe ki JAVA madhe Method table Picture madhe yeto ani CPP madhe nameMangling chi 
  concept picture madhe yete
 
 *jeva double-double vela tyach navane function Multi time lihil aasel teva Name Mangling
  chi concept used keli jate 

 *Name mangling he internally function ch nav change kerto
 *suppose fun(int x,int y) ===>>> internally funii/fun2i ya navane replace hot
 
 *ha compiler level la change kela jato
 
 *RTTI-(Runtime Type Identification):
            he ek ashi concept ahe ji ki - Runtime la thervte ki kontya type cha data yenar ahe

	    for example: int x=10;
	                 printf("%d\n",x);

			 std::cout<< x <<std::endl;

	'C' language madhe integer data assel ter "%d" ass aplyala sanagav lagat
	 pn 'CPP' madhe kontya type cha data ahe he sangav lagat nahi yach concept la RTTI ass mentat.

			 
 *Assemely Level lach Code chya function ch nav change kel jat
 *g++ -S program1.cpp ---->>>>> program1.s 
 ****program1.s hi file assembly language chi ahe ya file madhe function ch compiler ne change kel ahe 
      he disun yet
 *Assembly code madhe int fun(int x,int y)===>>> internally (_Z3funii / _Z3fun2i) ass jat
 CPU madhe ek concept ahe BigENDIAN ani littleENDIAN --->> kuthun data vachaycha ahe he sangat
 (CPU kontya way ne yenara data vachnar ahe he BigENDIAN ani LittleENDIAN yaver depend asst)
 * Jya Program madhe compiler nasto tyat interpreter assto
 * web development chya laguage madhe interpreter assto
 */

#include<iostream>

int add(int x,int y){//addii
	return x+y;
}
int add(int x,int y,int z){//addiii
	return x+y+z;
}
int main(){
	std::cout<<add(10,20)<<std::endl;//30
	std::cout<<add(10,20,20)<<std::endl;//50
}



