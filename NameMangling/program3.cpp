#include<iostream>

int add(int x,int y){
	return x+y;
}
int add(float x,int y){
	return x+y;
}
int main(){
	std::cout<<add(10.11,10)<<std::endl;
	return 0;
}
/*
 * output:
 *  error: call of overloaded ‘add(double, int)’ is ambiguous
   10 |  std::cout<<add(10.11,10)<<std::endl;
      |                         ^
program3.cpp:3:5: note: candidate: ‘int add(int, int)’
    3 | int add(int x,int y){
      |     ^~~
program3.cpp:6:5: note: candidate: ‘int add(float, int)’
    6 | int add(float x,int y){
      |     ^~~
*/
