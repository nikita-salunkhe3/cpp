#include<iostream>
#include<vector>

int main(){

	std::vector<int> v={10,20,30,40,50};

	//Iterator which is not Constant
	//By using iterator we can change the data
	for(auto itr=v.begin();itr != v.end(); itr++){
		*itr = *itr+50;
		std::cout<< *itr <<std::endl;
	}

	std::cout<< "*************************" <<std::endl;
	for(auto itr=v.begin(); itr!=v.cend(); itr++){
	
		*itr = *itr + 100;//No-error jeri begin() constant nahiye pn end() constant asel ter
		//error yet mahi........to data chnage karun deto

		std::cout<< *itr <<std::endl;
	}
	//Iterator can travel in reverse order
	for(auto itr=v.rbegin();itr != v.rend(); itr++){
		std::cout<< *itr <<std::endl;
	}

	//Iterator is Constant
	for(auto itr=v.cbegin(); itr!=v.cend(); itr++){
	
	//	*itr = *itr + 100;//error-Assignment of read-only Location -->>Karen object constant ahe

		std::cout<< *itr <<std::endl;
	}
}


