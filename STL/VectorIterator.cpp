//Java madhil Vector ani cpp madhil vector madhe ek major differece ahe
//java->Vector ==>> collection madhil serv object menun ch jata 
//cpp -> vector ===>> collection of Primitive datatype as well as Pre-defined object or User-defined
//object cha set assto

#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> vobj;

	vobj.push_back(10);
	vobj.push_back(20);
	vobj.push_back(30);
	vobj.push_back(40);
	vobj.push_back(50);

	std::vector<int>::iterator itr;//itr ha pointer ahe jo ki vector<int> type cha ahe

	for(itr=vobj.begin(); itr < vobj.end(); itr++){
		std::cout<< *itr <<std::endl;//10 20 30 40 50
	}
	return 0;
}
/*
 * ya madhe itr ha pointer ahe 
 * menun apan *(dereference) karun print kerto actual data*/
