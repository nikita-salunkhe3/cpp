#include<iostream>
#include<array>

int main(){

	std::array<int,5> a={10,20,30,40};

	std::array<int,5>::iterator itr;

	/*******Element Access********/
	//1.operator[]
	
	std::cout<< "Operator[] = " << a[1] <<std::endl;//20

	//2.at()
	
	std::cout<< "at() = " << a.at(1) <<std::endl;//20

	//3.front()
	
	std::cout<< "front() = " << a.front() <<std::endl;//10
	
	//4. end()
	
	std::cout<< "end() = " << a.end() <<std::endl;//0x1000 .....Address print hoto
	std::cout<< "end() = " << *(a.end()) <<std::endl;//0
	
	//5.back()
	
	std::cout<<"back = "<< a.back() <<std::endl;//0

	//6.data()
	
	std::cout<< "data = "<< a.data() <<std::endl;//0X100>>>>First Element cha address return kerto
	std::cout<< "data = "<< *(a.data()) <<std::endl;//10

	/***************array Modifiers**************/
	
        //7.fill()	
	a.fill(5);
	std::cout << "After fill method" << std::endl;

	for(itr = a.begin();itr != a.end();itr++){
		std::cout << *itr <<std::endl;//5 5 5 5 5
	}

	//8. swap()
	std::array<std::string,3> a1={"Nikita","Shweta","Ashwini"};
	std::array<std::string,3> a2={"Sakshi","Sanyogita","kirti"};

	a1.swap(a2);
	std::cout<< "After swap() method" <<std::endl;
	std::cout<< "In first array" <<std::endl;
	std::cout<< a1.size() <<std::endl;
/*
	std::array<int,3> a1={10,20,30};
	std::array<int,3> a2={50,60,70};

	a1.swap(a2);//compulsory doni array la same size dyavi lagte nahiter error yete
	std::cout<< "After swap() method" <<std::endl;
	std::cout<< "In first array" <<std::endl;
	std::cout<< a1.size() <<std::endl;
*/
	for(int i=0;i<a1.size();i++){
		std::cout<< a1.at(i) << "  ";
	}
	std::cout<< std::endl;

	std::cout<< "In Second array" <<std::endl;
	for(int i=0;i<a2.size();i++){
		std::cout<< a2.at(i) << "  ";
	}
	std::cout<< std::endl;

	/***************Capacity*******************/

	//9. size()
	
	std::cout<< "Size = "<< a.size() <<std::endl;//5

	//10. max_size
	
	std::cout<< "Max size = "<< a.max_size() <<std::endl;//5

	//11.empty()
	
	std::cout<< "Empty = " << a.empty() <<std::endl;//0

	/********************Iterator******************/

	//12.begin()
	//13.end()
	std::cout<< "Normal Iterator" <<std::endl;
	for(itr = a.begin();itr != a.end();itr++){
		std::cout << *itr <<std::endl;//[5,5,5,5,5]
	}

	//14.cbegin();
	//15.cend();
	std::cout<< "constant Iterator" <<std::endl;
	std::array<int,5>::const_iterator itr1;
	for(itr1 = a.cbegin();itr1 != a.cend();itr1++){
		std::cout<< *itr1 <<std::endl;//[5,5,5,5,5]
	}

	//16.rbegin();
	//17.rend();
	
	std::cout<< "Reverse Iterator" <<std::endl;
	std::array<int,5>::reverse_iterator itr2;
	for(itr2 = a.rbegin();itr2 != a.rend();itr2++){
		std::cout<< *itr2 <<std::endl;//[5,5,5,5,5]
	}

	//18.crbegin()
	//19.crend()
	std::cout<< "Constant Reverse Iterator" <<std::endl;
	std::array<int,5>::const_reverse_iterator itr3;
	for(itr3 = a.crbegin(); itr3 != a.crend(); itr3++){
		std::cout<< *itr3 <<std::endl;
	}

}


