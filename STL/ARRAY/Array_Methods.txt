﻿Array Methods

#include<iostream>
#include<array>

int main(){

	std::array<int,5> a={10,20,30,40};

	std::array<int,5>::iterator itr;

	/*******Element Access********/
	//1.operator[]
	
	std::cout<< "Operator[] = " << a[1] <<std::endl;//20

	//2.at()
	
	std::cout<< "at() = " << a.at(1) <<std::endl;//20

	//3.front()
	
	std::cout<< "front() = " << a.front() <<std::endl;//10
	
	//4. end()
	
	std::cout<< "end() = " << a.end() <<std::endl;//0x1000 .....Address print hoto
	std::cout<< "end() = " << *(a.end()) <<std::endl;//0
	
	//5.back()
	
	std::cout<<"back = "<< a.back() <<std::endl;//0

	//6.data()
	
	std::cout<< "data = "<< a.data() <<std::endl;//0X100>>>>First Element cha address return kerto
	std::cout<< "data = "<< *(a.data()) <<std::endl;//10

	/***************array Modifiers**************/
	
        //7.fill()	
	a.fill(5);
	std::cout << "After fill method" << std::endl;

	for(itr = a.begin();itr != a.end();itr++){
		std::cout << *itr <<std::endl;//5 5 5 5 5
	}

	//8. swap()
	std::array<std::string,3> a1={"Nikita","Shweta","Ashwini"};
	std::array<std::string,3> a2={"Sakshi","Sanyogita","kirti"};

	a1.swap(a2);
	std::cout<< "After swap() method" <<std::endl;
	std::cout<< "In first array" <<std::endl;
	std::cout<< a1.size() <<std::endl;
/*
	std::array<int,3> a1={10,20,30};
	std::array<int,3> a2={50,60,70};

	a1.swap(a2);//compulsory doni array la same size dyavi lagte nahiter error yete
	std::cout<< "After swap() method" <<std::endl;
	std::cout<< "In first array" <<std::endl;
	std::cout<< a1.size() <<std::endl;
*/
	for(int i=0;i<a1.size();i++){
		std::cout<< a1.at(i) << "  ";
	}
	std::cout<< std::endl;

	std::cout<< "In Second array" <<std::endl;
	for(int i=0;i<a2.size();i++){
		std::cout<< a2.at(i) << "  ";
	}
	std::cout<< std::endl;

	/***************Capacity*******************/

	//9. size()
	
	std::cout<< "Size = "<< a.size() <<std::endl;//5

	//10. max_size
	
	std::cout<< "Max size = "<< a.max_size() <<std::endl;//5

	//11.empty()
	
	std::cout<< "Empty = " << a.empty() <<std::endl;//0

	/********************Iterator******************/

	//12.begin()
	//13.end()
	std::cout<< "Normal Iterator" <<std::endl;
	for(itr = a.begin();itr != a.end();itr++){
		std::cout << *itr <<std::endl;//[5,5,5,5,5]
	}

	//14.cbegin();
	//15.cend();
	std::cout<< "constant Iterator" <<std::endl;
	std::array<int,5>::const_iterator itr1;
	for(itr1 = a.cbegin();itr1 != a.cend();itr1++){
		std::cout<< *itr1 <<std::endl;//[5,5,5,5,5]
	}

	//16.rbegin();
	//17.rend();
	
	std::cout<< "Reverse Iterator" <<std::endl;
	std::array<int,5>::reverse_iterator itr2;
	for(itr2 = a.rbegin();itr2 != a.rend();itr2++){
		std::cout<< *itr2 <<std::endl;//[5,5,5,5,5]
	}

	//18.crbegin()
	//19.crend()
	std::cout<< "Constant Reverse Iterator" <<std::endl;
	std::array<int,5>::const_reverse_iterator itr3;
	for(itr3 = a.crbegin(); itr3 != a.crend(); itr3++){
		std::cout<< *itr3 <<std::endl;
	}

}

                          Vector Methods

//vector class Methods
/*Functionality-
 * --Sequentially Memory allocation
 *  -we can access data by using index
 *  -Duplicate data chalto
 *  -we can resize vector size
 */

#include<iostream>
#include<vector>

int main(){
	std::vector<int> v = {10,20,30,40};

	std::vector<int>::iterator itr;

	//1. size()
	std::cout<< "Size = " << v.size() <<std::endl;
	
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout << *itr <<std::endl;//10 20 30 40
	}
	//2. void assign (size_type n, const value_type& val);
	v.assign(4,100);
	
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout << *itr <<std::endl;// 100 100 100 100
	}

	std::cout<< "Size = " << v.size() <<std::endl;//4
	//3. at(size_type n) ==>> (index parameter) tya index Paramter varil data return kerto
   	std::cout<< v.at(2) <<std::endl;

	//4.push_back(value);
	
	v.push_back(200);

	//5.back()
	std::cout << v.back() <<std::endl;//200

	//6.capacity()
	
	std::cout<< v.capacity() <<std::endl;//8

	//7.max_size()
	std::cout << "Max Size : " << v.max_size() <<std::endl;//19 character size=>2305843009213693951

	//8.resize()
	v.resize(8);
	std::cout<< "Resize " <<std::endl;// 100 100 100 100 200 0 0 0

	v.resize(10,400);
	std::cout<< "Resize " <<std::endl;// 100 100 100 100 200 0 0 0 400 400

/*	v.resize(-3);//error -->>terminate called after throwing an instance of 'std::length_error'
  what():  vector::_M_default_append
Aborted (core dumped)*/


	//9.empty()
	std::cout <<"Empty "<< v.empty() <<std::endl;//0

	//10.reserve()
	std::cout<< "Reserve "  <<std::endl;
	v.reserve(20);

	std::cout<< "Capacity :" << v.capacity() <<std::endl;//20

	//11. shrink_to_fit()
	std::cout<< "Shrink_to_fit " <<std::endl;
	v.shrink_to_fit();

	std::cout<< "Capacity :" << v.capacity() <<std::endl;//10

	//12.data()
	
	std::cout<< "Data() = " << *(v.data()) <<std::endl;//100 -->>it returns first data of vector 
	
	//13.operator[]
	
	std::cout<< "Operator[] = "<< v[8] <<std::endl;//400

	//14.at(index)
	
	std::cout<< "at() = " << v.at(1) <<std::endl;//100

	//15. insert()
	
	v.insert((v.begin()), 500);
	std::cout<< "Insert" <<std::endl;

	//************Methods of Iterator************
	
	//Iterator
	//1.begin
	//2.end
	
	for(itr=v.begin(); itr != v.end(); itr++){
		std::cout<< *itr <<std::endl;// 500 100 100 100 100 200 0 0 0 400 400 
	}
	std::vector<int>::reverse_iterator itr1;

	for(itr1 = v.rbegin(); itr1 != v.rend(); itr1++){
		std::cout<< *itr1 <<std::endl;// 400 400 0 0 0 200 100 100 100 100 500
	}

	std::cout<< *(v.begin()) <<std::endl;// 500
	std::cout<< "End is :" << *(v.end()) <<std::endl;//0

	//3.cbegin
	//4.cend
	
	std::vector<int>::const_iterator itr2;

	for(itr2 = v.cbegin() ; itr2 != v.cend(); itr2++){
		std::cout<< *itr2 <<std::endl;//500 100 100 100 100 200 0 0 0 400 400 
	}
	
	//5.crbegin()
	//6.crend()
	
	std::vector<int>::const_reverse_iterator itr3;

	for(itr3 = v.crbegin();itr3 != v.crend();itr3++){
		std::cout<< *itr3 <<std::endl;// 400 400 0 0 0 200 100 100 100 100 500
	}
	
	//16.erase
	v.erase(v.begin()+1,v.begin()+4);
	std::cout<< "after erase ";
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 0 0 0 400 400
	}
	std::cout<< std::endl;

	v.erase(v.begin()+5);
	std::cout<<"after 5th data erase" <<std::endl;
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 0 0 400 400
	}
	std::cout<< std::endl;

	//17.swap()
	std::vector<std::string> v1={"Nikita","Shweta","Shradha"};
	std::vector<std::string> v2={"Kirti","Sanyogita","Sakshi"};

	v1.swap(v2);

	std::cout<< "V1 vector contains:" <<std::endl;
	for(int i=0;i<v1.size();i++){
		std::cout<< v1[i] <<"   ";
	}
	std::cout<< std::endl;
	
	std::cout<< "V2 vector contains:" <<std::endl;
	for(int i=0;i<v2.size();i++){
		std::cout<< v2[i] <<"   ";
	}
	std::cout<< std::endl;

	//18.emplace()//required 2 paramter 
	v.emplace(v.begin()+3, 300);
	std::cout<<"After emplace "<<std::endl;
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 300 0 0 400 400
	}
	std::cout<< std::endl;

	//19.emplace_back().........last position la data insert karun deil
	v.emplace_back(600);
	std::cout<< "After emplace_back " <<std::endl;
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 300 0 0 400 400 600
	}
	std::cout<< std::endl;

	//20.clear();
	std::cout<< "Before Clear" <<std::endl;
	std::cout<< v.empty() <<std::endl;//0
	v.clear();
	std::cout<< "After clear" <<std::endl;
	std::cout<< v.empty() <<std::endl;//1
	return 0;
}

                              List Methods

#include<iostream>
#include<list>

bool single_digit(const int& num){
	return (num<10 && num>-9);
}
bool is_even(const int& num){
	return (num%2 == 0);
}

int main(){
	std::list<int> l={10,20,30,40,50};

	std::list<int>::iterator itr;
	/***************Capacity***********/

	//1.empty()
	std::cout<< "Empty() = "<< l.empty() <<std::endl;//0

	//2.size()
	std::cout<< "size() = "<< l.size() <<std::endl;//5

	//3.max_size()
	std::cout<< "max_size() = "<< l.max_size() <<std::endl;//384307168202282325

	/***************Element Access*********/
	//4.front()
	std::cout << "front = "<< l.front() <<std::endl;//10

	//5.back()
	std::cout << "back = "<<l.back() <<std::endl;//50

	/****************Modifiers************/
	//6.assign()
	std::list<int> obj;
	obj.assign(5,100);
	std::cout<< "After assign() "<<std::endl;
	for(itr = obj.begin();itr != obj.end();itr++){
		std::cout<< *itr << "   ";//100 100 100 100 100
	}
	std::cout<<std::endl;

	//7.emplace_front()
	l.emplace_front(5);
	std::cout<< "After emplace_front() "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";//5 10 20 30 40 50
	}
	std::cout<<std::endl;

	//8.push_front(value)
	
	l.push_front(2);
	std::cout<< "After push_front(value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 2 5 10 20 30 40 50
	}
	std::cout<<std::endl;

	//9.pop_front()
	
	l.pop_front();
	std::cout<< "After pop_front() "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50
	}
	std::cout<<std::endl;

	//10.emplace_back()
	
	l.emplace_back(60);
	std::cout<< "After emplace_back(value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60
	}
	std::cout<<std::endl;
	//11.push_back(value)
	l.push_back(70);
	std::cout<< "After push_back(value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 70 
	}
	std::cout<<std::endl;
	//12.pop_back()
	
	l.pop_back();
	std::cout<< "After pop_back() "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 
	}
	std::cout<<std::endl;
	//13.emplace(Position,Value);
	l.emplace(l.begin(),77);
	std::cout<< "After emplace(first_element_address,value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 
	}
	std::cout<<std::endl;

	l.emplace(l.end(),88);
	std::cout<< "After emplace(Last_element_address,value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 77 5 10 20 30 40 50 60 88
	}
	std::cout<<std::endl;

	//14.insert()
	l.insert(l.end(),99);
	std::cout<< "After insert(Last_element_address,value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 77 5 10 20 30 40 50 60 88 99
	}
	std::cout<<std::endl;
	
	//15.erase(pointer)
	l.erase(l.begin());
	std::cout<< "After erase(iterator (pointer)) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 88 99
	}
	std::cout<<std::endl;

	//16.swap()
	std::list<char> ch1={'A','B','C'};
	std::list<char> ch2={'a','b','c'};

	std::list<char>::iterator itr1;
	ch1.swap(ch2);
	std::cout<< "After swap() "<<std::endl;
	for(itr1 = ch1.begin();itr1 != ch1.end();itr1++){
		std::cout<< *itr1 << "   ";// a b c
	}
	std::cout<<std::endl;
	for(itr1 = ch2.begin();itr1 != ch2.end();itr1++){
		std::cout<< *itr1 << "   ";// A  B  C
	}
	std::cout<<std::endl;

	//17.resize()
	std::cout<< "before size of list = "<< l.size() <<std::endl;//9
	l.resize(15);
	std::cout<< "After resize size of list = "<< l.size() <<std::endl;//15

	/****************Operations on List***********/
	//18.splice(position,list)
	std::list<std::string> list1={"Nikita","Shweta","Shreya"}; 
	std::list<std::string> list2={"Sanyogita","Kirti","Sakshi"};
	
	std::list<std::string>::iterator str;

	list1.splice(list1.end(),list2);

	std::cout<< "After splice() "<<std::endl;
	for(str = list1.begin();str != list1.end();str++){
		std::cout<< *str << "   ";//Nikita Shweta Shreya Sanyogita Kirti Sakshi
	}
	std::cout<<std::endl;
	for(str = list2.begin();str != list2.end();str++){
		std::cout<< *str << "   ";//(Blank)
	}
	std::cout<<std::endl;
	
	std::list<std::string> list3={"1","2","3"}; 
	list1.splice(list3.begin(),list1);

	std::cout<< "After splice() "<<std::endl;
	for(str = list1.begin();str != list1.end();str++){
		std::cout<< *str << "   ";// Sanyogita Kirti Sakshi Nikita Shweta Shreya
	}

	std::cout<<std::endl;
	for(str = list3.begin();str != list3.end();str++)
	{
		std::cout<< *str << "   ";//Sanyogita kirti Sakshi Nikita Shweta Shreya 1 2 3
	}
	std::cout<< std::endl;

	//19.remove(value);
	l.remove(88);
	std::cout<< "After remove" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 99 0 0 0 0 0 0 0
	}
	std::cout<<std::endl;
	
	//20.remove_if()
	
	l.remove_if(single_digit);
	std::cout<< "After remove_if(single_digit)" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 10 20 30 40 50 60 99 
	}
	std::cout<<std::endl;
	
	l.remove_if(is_even);
	std::cout<< "After remove_if(is_odd)" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 99 
	}
	std::cout<<std::endl;

	l.push_back(10);
	l.push_back(20);
	l.push_back(10);

	//21.unique()
	
	std::cout<< "Before unique()" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 99 10 20 10
	}
	std::cout<<std::endl;

	l.sort();
	l.unique();

	std::cout<< "After unique()" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 10 20 99
	}
	std::cout<<std::endl;
	//22.merge
	
	ch1.merge(ch2);
	std::cout<< "After merge() "<<std::endl;
	for(itr1 = ch1.begin();itr1 != ch1.end();itr1++){
		std::cout<< *itr1 << "   ";// A B C a b c
	}
	std::cout<<std::endl;
	//23.sort()
	
	std::list<int> l2={5,32,65,12,98,75,1};
	l2.sort();
	std::cout<< "After sort()" <<std::endl;
	for(itr = l2.begin();itr != l2.end();itr++){
		std::cout<< *itr << "   ";//1,5,12,32,65,75,98
	}
	std::cout<<std::endl;
	//24.reverse()
	
	l2.reverse();
	std::cout<< "After reverse()" <<std::endl;
	for(itr = l2.begin();itr != l2.end();itr++){
		std::cout<< *itr << "   ";//98,75,65,32,12,5,1
	}
	std::cout<<std::endl;

	//25.clear();
	l2.clear();
	std::cout<< "After clear()" <<std::endl;
	for(itr = l2.begin();itr != l2.end();itr++){
		std::cout<< *itr << "   ";//(Blank)
	}
	std::cout<<std::endl;

}
