#include<iostream>
#include<list>

bool single_digit(const int& num){
	return (num<10 && num>-9);
}
bool is_even(const int& num){
	return (num%2 == 0);
}

int main(){
	std::list<int> l={10,20,30,40,50};

	std::list<int>::iterator itr;
	/***************Capacity***********/

	//1.empty()
	std::cout<< "Empty() = "<< l.empty() <<std::endl;//0

	//2.size()
	std::cout<< "size() = "<< l.size() <<std::endl;//5

	//3.max_size()
	std::cout<< "max_size() = "<< l.max_size() <<std::endl;//384307168202282325

	/***************Element Access*********/
	//4.front()
	std::cout << "front = "<< l.front() <<std::endl;//10

	//5.back()
	std::cout << "back = "<<l.back() <<std::endl;//50

	/****************Modifiers************/
	//6.assign()
	std::list<int> obj;
	obj.assign(5,100);
	std::cout<< "After assign() "<<std::endl;
	for(itr = obj.begin();itr != obj.end();itr++){
		std::cout<< *itr << "   ";//100 100 100 100 100
	}
	std::cout<<std::endl;

	//7.emplace_front()
	l.emplace_front(5);
	std::cout<< "After emplace_front() "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";//5 10 20 30 40 50
	}
	std::cout<<std::endl;

	//8.push_front(value)
	
	l.push_front(2);
	std::cout<< "After push_front(value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 2 5 10 20 30 40 50
	}
	std::cout<<std::endl;

	//9.pop_front()
	
	l.pop_front();
	std::cout<< "After pop_front() "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50
	}
	std::cout<<std::endl;

	//10.emplace_back()
	
	l.emplace_back(60);
	std::cout<< "After emplace_back(value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60
	}
	std::cout<<std::endl;
	//11.push_back(value)
	l.push_back(70);
	std::cout<< "After push_back(value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 70 
	}
	std::cout<<std::endl;
	//12.pop_back()
	
	l.pop_back();
	std::cout<< "After pop_back() "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 
	}
	std::cout<<std::endl;
	//13.emplace(Position,Value);
	l.emplace(l.begin(),77);
	std::cout<< "After emplace(first_element_address,value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 
	}
	std::cout<<std::endl;

	l.emplace(l.end(),88);
	std::cout<< "After emplace(Last_element_address,value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 77 5 10 20 30 40 50 60 88
	}
	std::cout<<std::endl;

	//14.insert()
	l.insert(l.end(),99);
	std::cout<< "After insert(Last_element_address,value) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 77 5 10 20 30 40 50 60 88 99
	}
	std::cout<<std::endl;
	
	//15.erase(pointer)
	l.erase(l.begin());
	std::cout<< "After erase(iterator (pointer)) "<<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 88 99
	}
	std::cout<<std::endl;

	//16.swap()
	std::list<char> ch1={'A','B','C'};
	std::list<char> ch2={'a','b','c'};

	std::list<char>::iterator itr1;
	ch1.swap(ch2);
	std::cout<< "After swap() "<<std::endl;
	for(itr1 = ch1.begin();itr1 != ch1.end();itr1++){
		std::cout<< *itr1 << "   ";// a b c
	}
	std::cout<<std::endl;
	for(itr1 = ch2.begin();itr1 != ch2.end();itr1++){
		std::cout<< *itr1 << "   ";// A  B  C
	}
	std::cout<<std::endl;

	//17.resize()
	std::cout<< "before size of list = "<< l.size() <<std::endl;//9
	l.resize(15);
	std::cout<< "After resize size of list = "<< l.size() <<std::endl;//15

	/****************Operations on List***********/
	//18.splice(position,list)
	std::list<std::string> list1={"Nikita","Shweta","Shreya"}; 
	std::list<std::string> list2={"Sanyogita","Kirti","Sakshi"};
	
	std::list<std::string>::iterator str;

	list1.splice(list1.end(),list2);

	std::cout<< "After splice() "<<std::endl;
	for(str = list1.begin();str != list1.end();str++){
		std::cout<< *str << "   ";//Nikita Shweta Shreya Sanyogita Kirti Sakshi
	}
	std::cout<<std::endl;
	for(str = list2.begin();str != list2.end();str++){
		std::cout<< *str << "   ";//(Blank)
	}
	std::cout<<std::endl;
	
	std::list<std::string> list3={"1","2","3"}; 
	list1.splice(list3.begin(),list1);

	std::cout<< "After splice() "<<std::endl;
	for(str = list1.begin();str != list1.end();str++){
		std::cout<< *str << "   ";// Sanyogita Kirti Sakshi Nikita Shweta Shreya
	}

	std::cout<<std::endl;
	for(str = list3.begin();str != list3.end();str++)
	{
		std::cout<< *str << "   ";//Sanyogita kirti Sakshi Nikita Shweta Shreya 1 2 3
	}
	std::cout<< std::endl;

	//19.remove(value);
	l.remove(88);
	std::cout<< "After remove" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 5 10 20 30 40 50 60 99 0 0 0 0 0 0 0
	}
	std::cout<<std::endl;
	
	//20.remove_if()
	
	l.remove_if(single_digit);
	std::cout<< "After remove_if(single_digit)" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 10 20 30 40 50 60 99 
	}
	std::cout<<std::endl;
	
	l.remove_if(is_even);
	std::cout<< "After remove_if(is_odd)" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 99 
	}
	std::cout<<std::endl;

	l.push_back(10);
	l.push_back(20);
	l.push_back(10);

	//21.unique()
	
	std::cout<< "Before unique()" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 99 10 20 10
	}
	std::cout<<std::endl;

	l.sort();
	l.unique();

	std::cout<< "After unique()" <<std::endl;
	for(itr = l.begin();itr != l.end();itr++){
		std::cout<< *itr << "   ";// 10 20 99
	}
	std::cout<<std::endl;
	//22.merge
	
	ch1.merge(ch2);
	std::cout<< "After merge() "<<std::endl;
	for(itr1 = ch1.begin();itr1 != ch1.end();itr1++){
		std::cout<< *itr1 << "   ";// A B C a b c
	}
	std::cout<<std::endl;
	//23.sort()
	
	std::list<int> l2={5,32,65,12,98,75,1};
	l2.sort();
	std::cout<< "After sort()" <<std::endl;
	for(itr = l2.begin();itr != l2.end();itr++){
		std::cout<< *itr << "   ";//1,5,12,32,65,75,98
	}
	std::cout<<std::endl;
	//24.reverse()
	
	l2.reverse();
	std::cout<< "After reverse()" <<std::endl;
	for(itr = l2.begin();itr != l2.end();itr++){
		std::cout<< *itr << "   ";//98,75,65,32,12,5,1
	}
	std::cout<<std::endl;

	//25.clear();
	l2.clear();
	std::cout<< "After clear()" <<std::endl;
	for(itr = l2.begin();itr != l2.end();itr++){
		std::cout<< *itr << "   ";//(Blank)
	}
	std::cout<<std::endl;

}
