#include<iostream>
#include<vector>

int main(){

	std::vector<int> obj;

	obj.push_back(10);
	obj.push_back(20);
	obj.push_back(30);
	obj.push_back(40);

	std::vector<int>::iterator itr;

	//iterator which is Non-constant
	for(itr=obj.begin();itr != obj.end;itr++){
		*itr = *itr+100;
		std::cout<< *itr <<std::endl;
	}

	//Iterator travels in reverse order
	for(itr=obj.rbegin();itr != rend(); itr++){
		std::cout<< *itr <<std::endl;
	}

	//Iterator which is constant
	for(itr=obj.cbegin();itr != std::cend();itr++){
		*itr = *itr+1000;//error- Assignment of read-only location
		//karen Iterator ha apla constant type cha ahe tyamule iterator cha madtine data change 
		//karu shakat nahi
		std::cout<< *itr <<std::endl;
	}
	return 0;
}
