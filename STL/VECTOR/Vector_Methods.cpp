//vector class Methods
/*Functionality-
 * --Sequentially Memory allocation
 *  -we can access data by using index
 *  -Duplicate data chalto
 *  -we can resize vector size
 */

#include<iostream>
#include<vector>

int main(){
	std::vector<int> v = {10,20,30,40};

	std::vector<int>::iterator itr;

	//1. size()
	std::cout<< "Size = " << v.size() <<std::endl;
	
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout << *itr <<std::endl;//10 20 30 40
	}
	//2. void assign (size_type n, const value_type& val);
	v.assign(4,100);
	
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout << *itr <<std::endl;// 100 100 100 100
	}

	std::cout<< "Size = " << v.size() <<std::endl;//4
	//3. at(size_type n) ==>> (index parameter) tya index Paramter varil data return kerto
   	std::cout<< v.at(2) <<std::endl;

	//4.push_back(value);
	
	v.push_back(200);

	//5.back()
	std::cout << v.back() <<std::endl;//200

	//6.capacity()
	
	std::cout<< v.capacity() <<std::endl;//8

	//7.max_size()
	std::cout << "Max Size : " << v.max_size() <<std::endl;//19 character size=>2305843009213693951

	//8.resize()
	v.resize(8);
	std::cout<< "Resize " <<std::endl;// 100 100 100 100 200 0 0 0

	v.resize(10,400);
	std::cout<< "Resize " <<std::endl;// 100 100 100 100 200 0 0 0 400 400

/*	v.resize(-3);//error -->>terminate called after throwing an instance of 'std::length_error'
  what():  vector::_M_default_append
Aborted (core dumped)*/


	//9.empty()
	std::cout <<"Empty "<< v.empty() <<std::endl;//0

	//10.reserve()
	std::cout<< "Reserve "  <<std::endl;
	v.reserve(20);

	std::cout<< "Capacity :" << v.capacity() <<std::endl;//20

	//11. shrink_to_fit()
	std::cout<< "Shrink_to_fit " <<std::endl;
	v.shrink_to_fit();

	std::cout<< "Capacity :" << v.capacity() <<std::endl;//10

	//12.data()
	
	std::cout<< "Data() = " << *(v.data()) <<std::endl;//100 -->>it returns first data of vector 
	
	//13.operator[]
	
	std::cout<< "Operator[] = "<< v[8] <<std::endl;//400

	//14.at(index)
	
	std::cout<< "at() = " << v.at(1) <<std::endl;//100

	//15. insert()
	
	v.insert((v.begin()), 500);
	std::cout<< "Insert" <<std::endl;

	//************Methods of Iterator************
	
	//Iterator
	//1.begin
	//2.end
	
	for(itr=v.begin(); itr != v.end(); itr++){
		std::cout<< *itr <<std::endl;// 500 100 100 100 100 200 0 0 0 400 400 
	}
	std::vector<int>::reverse_iterator itr1;

	for(itr1 = v.rbegin(); itr1 != v.rend(); itr1++){
		std::cout<< *itr1 <<std::endl;// 400 400 0 0 0 200 100 100 100 100 500
	}

	std::cout<< *(v.begin()) <<std::endl;// 500
	std::cout<< "End is :" << *(v.end()) <<std::endl;//0

	//3.cbegin
	//4.cend
	
	std::vector<int>::const_iterator itr2;

	for(itr2 = v.cbegin() ; itr2 != v.cend(); itr2++){
		std::cout<< *itr2 <<std::endl;//500 100 100 100 100 200 0 0 0 400 400 
	}
	
	//5.crbegin()
	//6.crend()
	
	std::vector<int>::const_reverse_iterator itr3;

	for(itr3 = v.crbegin();itr3 != v.crend();itr3++){
		std::cout<< *itr3 <<std::endl;// 400 400 0 0 0 200 100 100 100 100 500
	}
	
	//16.erase
	v.erase(v.begin()+1,v.begin()+4);
	std::cout<< "after erase ";
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 0 0 0 400 400
	}
	std::cout<< std::endl;

	v.erase(v.begin()+5);
	std::cout<<"after 5th data erase" <<std::endl;
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 0 0 400 400
	}
	std::cout<< std::endl;

	//17.swap()
	std::vector<std::string> v1={"Nikita","Shweta","Shradha"};
	std::vector<std::string> v2={"Kirti","Sanyogita","Sakshi"};

	v1.swap(v2);

	std::cout<< "V1 vector contains:" <<std::endl;
	for(int i=0;i<v1.size();i++){
		std::cout<< v1[i] <<"   ";
	}
	std::cout<< std::endl;
	
	std::cout<< "V2 vector contains:" <<std::endl;
	for(int i=0;i<v2.size();i++){
		std::cout<< v2[i] <<"   ";
	}
	std::cout<< std::endl;

	//18.emplace()//required 2 paramter 
	v.emplace(v.begin()+3, 300);
	std::cout<<"After emplace "<<std::endl;
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 300 0 0 400 400
	}
	std::cout<< std::endl;

	//19.emplace_back().........last position la data insert karun deil
	v.emplace_back(600);
	std::cout<< "After emplace_back " <<std::endl;
	for(itr = v.begin(); itr != v.end(); itr++){
		std::cout<< *itr << "  ";// 500 100 200 300 0 0 400 400 600
	}
	std::cout<< std::endl;

	//20.clear();
	std::cout<< "Before Clear" <<std::endl;
	std::cout<< v.empty() <<std::endl;//0
	v.clear();
	std::cout<< "After clear" <<std::endl;
	std::cout<< v.empty() <<std::endl;//1
	return 0;
}
