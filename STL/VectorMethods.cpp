/***Methods of vector class**********
1. front() => First data print karun deil
2. size()
3. return type-> value/data obj[index]
4. data()
5. back() => last data print karun deil

*/

#include<iostream>
#include<vector>

int main(){
	std::vector<std::string> v = {"Nikita","sarthak","shweta","Aakash"};

	std::vector<std::string>::iterator itr;

//	std::cout<< itr <<std::endl;//error- No matching function call for operator<<cout,pointer 

	std::cout<< v.size() <<std::endl;//4
	std::cout<< v[0] <<std::endl;//Nikita
	std::cout<< v[1] <<std::endl;//sarthak
	std::cout<< v[2] <<std::endl;//shweta
	std::cout<< v[3] <<std::endl;//Aakash
//	std::cout<< v[4] <<std::endl;//Runtime error- Segmentation fault(core dumped)

	std::cout<< v.front() <<std::endl;//Nikita
	std::cout<< v.back() <<std::endl;
	return 0;
}
