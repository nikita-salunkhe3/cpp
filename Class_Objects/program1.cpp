/*class ha pn ek Structure ch ahe 
 * CPP madhe bydefault class madhil member he private asstat
 * structure ka vapraych karen various datatype he ekach navat thevle jatat menun
 
 Differnec between Struture in C and Structure in CPP
 1. CPP madhe value assign kerta yetat tas C madhe value structure cha aat assign kerta yet nahit
 2. CPP madhe Structure madhe function lihilta yetat pn C madhe Function structure cha aat lihita
    yet nahit
 3. CPP madhe Structure cha navane object tayar kela jato pn C madhe (struct name obj;) assa Structure 
    ne object tayar kela jato
 */

#include<iostream>
struct Player{
	int jerNo=18;
	char name[20]="Virat Kolhi";

	void disp(){
		std::cout<<jerNo<<std::endl;
		std::cout<<name<<std::endl;
	}
};
int main(){
	Player obj;

	std::cout<<obj.jerNo<<std::endl;
	std::cout<<obj.name<<std::endl;

	obj.disp();
	return 0;
}


