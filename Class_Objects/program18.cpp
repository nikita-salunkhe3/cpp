#include<iostream>
class Demo{
	public:
	static void gun(){
		static int x=10;
		int y=20;

		std::cout<< x <<std::endl;
		std::cout<< y <<std::endl;
	/*	std::cout<< this <<std::endl;//error:‘this’ is unavailable for static member functions
   10 |   std::cout<< this <<std::endl;
      |               ^~~~
*/
	}
};
int main(){
	Demo obj;

	obj.gun();

	std::cout<< &obj <<std::endl;

	return 0;
}
