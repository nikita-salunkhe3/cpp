#include<iostream>
class Demo{
	public:
		Demo(){
			std::cout<<"No-args"<<std::endl;
		}
		Demo(int x){
			std::cout<<"Para"<<std::endl;
		}
		Demo(Demo &obj){
			std::cout<<"Copy Constructor"<<std::endl;
		}
};
int main(){
	Demo obj1;
	Demo obj2;
	Demo obj3;

	Demo arr[]={obj1,obj2,obj3};
	return 0;
}
