#include<iostream>

class Company{
	int countEmp=5000;
	std::string name="IBM";

	public:
	Company(){
		std::cout<< "The company Constructor"<<std::endl;
	}
	void compInfo(){
		std::cout<< countEmp << std::endl;
		std::cout<<name << std::endl;
	}
};
class Employee{
	int empId=10;
	float empSal=90.09f;

	public:
	Employee(){
		std::cout<< "In Employee Constructor"<<std::endl;
	}
	void empInfo(){
		Company obj;

		std::cout<< empId <<std::endl;
		std::cout<<empSal<<std::endl;

	//	std::cout<<obj.countEmp<<std::endl;//error
	//	std::cout<<obj.name<<std::endl;//error 
		//direct access nahi kerta yenar karen te vercha class madhe private ahet
		//ami private gosti object ne sudha baherun access kerta yet nahi
		//tyamule error aali
	}
};
int main(){
	Employee *emp=new Employee();
	emp->empInfo();
	return 0;
}
/*
 * output:
 * In Employee Constructor
 * The company Constructor
 * 10
 * 90.0
 */


