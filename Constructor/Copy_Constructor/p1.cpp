#include<iostream>
class Demo{

	public:
	Demo(){
		std::cout<<"In No-args"<<std::endl;
	}
	Demo(int x){
		std::cout<< "In Para" <<std::endl;
	}
	Demo(Demo& xyz){
		std::cout<<"In copy"<<std::endl;
	}
};
int main(){
	Demo obj1;
	Demo *obj2=new Demo();

	Demo *obj3=obj2;
	return 0;
}
