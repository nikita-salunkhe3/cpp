#include<iostream>
class Demo{
	public:
		Demo(){
			std::cout<<"No-args Constructor"<<std::endl;
		}
		Demo(int x){
			std::cout<<"Para Constructor"<<std::endl;
			std::cout<< x <<std::endl;
//			Demo obj1;
//			Demo obj2=obj1;
		}
		Demo(Demo &xyz){
			std::cout<<"Copy constructor"<<std::endl;
		}
};
int main(){
	Demo obj1;
//	Demo obj2(10);
	Demo obj3(obj1);
//	Demo obj4=obj1;  
}


