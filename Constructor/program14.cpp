#include<iostream>
class Demo{

	int x;
	int y;

	public:
	Demo(){
		std::cout<< "In No-args constructor" <<std::endl;
	}
	Demo(int x,int y){
		std::cout<< "In Para constructor" <<std::endl;
		std::cout<< this->x <<std::endl;//GV
		std::cout<< this->y <<std::endl;//GV
		this->x=x;
		this->y=y;
		std::cout<< this->x <<std::endl;//10
		std::cout<< this->y <<std::endl;//20
	}
};
int main(){
	Demo obj{10,20};
	return 0;
}
