#include<iostream>
class Demo{
	int x=10;
	int y=20;

	protected:
	int z=30;

	//public:
	
	void Disp(){
		std::cout<< x << y <<std::endl;
	}
};
int main(){
	Demo obj;
	obj.Disp();

}
/*
 * output:
 * Disp() is protected within this context
 */
