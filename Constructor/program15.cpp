#include<iostream>
class Demo{
	public:
		Demo(){
			std::cout<< "In No-args" <<std::endl;
		}
		Demo(Demo& xyz){
			std::cout<< "In copy constructor" <<std::endl;
		}
		Demo(Demo* &xyz){
			std::cout<< "In Pointer copy constructor" <<std::endl;
		}

};
int main(){
	Demo obj1;
	Demo *obj2=new Demo();

	Demo obj3(obj1);
	Demo obj4(obj2);
	return 0;
}

