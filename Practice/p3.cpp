#include<iostream>
void fun(int x,int y){
	std::cout<<"In Normal Fun"<<std::endl;

}
void fun(int &ref1, int &ref2){

	int temp=ref1;
	ref1=ref2;
	ref2=temp;
}
int main(){
	int x=10;
	int y=20;

//	fun(x,y);
	fun(50,100);//In Normal Fun
}
/*
 * error: call of overloaded ‘fun(int&, int&)’ is ambiguous
   15 |  fun(x,y);
      |         ^
p3.cpp:2:6: note: candidate: ‘void fun(int, int)’
    2 | void fun(int x,int y){
      |      ^~~
p3.cpp:6:6: note: candidate: ‘void fun(int&, int&)’
    6 | void fun(int &ref1, int &ref2){
      |      ^~~

 */
