/*
 Birth of CPP :- 1978 and Standarized in 1998
 Founder of CPP :- Bjarne Stroustrup
 Microsoft makes its own cpp that is "COM" (Component Object Model)
 Standarized(Stable) version of CPP is 03
 CPP internally calls to C
 CPP versions (2003,2007,2011,2014,2017,2020,2023(Launching in Dec))

 If we write using namespace line we don't need to write std:: for e.g(we need not to write std::cout 
we can write as cout)
 

*/

#include<iostream>

using namespace std;

int main(){
	cout<<"Nik"<<endl;

	int x;

	cin>>x;

	cout<<x<<endl;

	return 0;
}

