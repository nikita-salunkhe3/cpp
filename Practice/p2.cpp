/*
 * cout is the object of iostream class
 * if we write cout cin as a variable and print it then it does not gives an error it will be treat as
 * normal variable  
 */
#include<iostream>
using namespace std;
int main(){

	int cout=10;
	int cin=20;

//	std::cout<<cout<<std::endl;
//	std::cout<<cin<<std::endl;
	
	cout<<cout<<endl;
	cout<<cin<<endl;
}
