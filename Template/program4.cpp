#include<iostream>
template<typename T>
struct Node{
	T data;
	Node *next;
};
template<class T>
class SSLinkedList{
	Node <T> *head=NULL;

	public:
	int countNode(){
		Node<T> *temp=head;
		int count=0;
		while(temp->next != head){
			count++;
			temp=temp->next;
		}
		return count;
	}

	Node <T> *createNode(){
		Node <T> *newnode=(Node<T>*)malloc(sizeof(Node<T>));

		std::cout<<"Enter the data"<<std::endl;
		std::cin>>newnode->data;

		newnode->next=head;
		return newnode;
	}
	void addNode(){
		Node<T> *newnode=createNode();
		if(head==NULL){
			head=newnode;
			newnode->next=head;
		}else{
			Node<T> *temp=head;
			while(temp->next != head){
				temp=temp->next;
			}
			temp->next=newnode;
			newnode->next=head;
		}
	}
	void addFirst(){
		Node<T> *newnode=createNode();

		if(head==NULL){
			head=newnode;
			newnode->next=head;
		}else{
			newnode->next=head;
			head=newnode;
		}
	}
	void addLast(){
		Node<T> *newnode=createNode();
		if(head==NULL){
			head=newnode;
			newnode->next=head;
		}else{
			Node<T> *temp=head;
			while(temp->next != head){
				temp=temp->next;
			}
			temp->next=newnode;
			newnode->next=head;
		}
	}
	int addAtPos(int pos){
		int count=countNode();
		if(pos<=0 || pos>count+1){
			return -1;
		}else{
			Node<T> *newnode=createNode();
			if(pos==1){
				addFirst();
			}else if(pos==count+1){
				addLast();
			}else{
				Node<T> *temp=head;
				while(pos-2){
					temp=temp->next;
					pos--;
				}
				newnode->next=temp->next;
				temp->next=newnode;
			}
		}
	}

	int deleteFirst(){
		if(head==NULL){
			return -1;
		}else if(head->next==head){
			free(head);
			head=NULL;
		}else{
			Node<T> *temp=head;
                        while(temp->next != head){
                                temp=temp->next;
                        }

			head=head->next;
			free(temp->next);
			temp->next=head;
		}
	}
	int deleteLast(){
		if(head==NULL){
			return -1;
		}else if(head->next==head){
			free(head);
			head=NULL;
		}else{
			Node<T> *temp=head;
			while(temp->next->next != head){
				temp=temp->next;
			}
		
			free(temp->next);
			temp->next=head;
		}
	}
	int deleteAtPos(int pos){
		int count=countNode();
		if(pos<=0 || pos>count+1){
			return -1;
		}else{
			if(pos==1){
				deleteFirst();
			}else if(pos==count+1){
				deleteLast();
			}else{
				Node<T> *temp1=head;
				Node<T> *temp2=NULL;
				while(pos-2){
					temp1=temp1->next;
					pos--;
				}
				temp2=temp1->next;
				temp1->next=temp2->next;
				free(temp2);
			}
		}
	}
	int printSLL(){
		if(head==NULL){
			return -1;
		}else{
			Node<T> *temp=head;
			while(temp->next != head){
				std::cout<< temp->data <<std::endl;
				temp=temp->next;
			}
			std::cout<< temp->data <<std::endl;
		}
	}
};
int main(){
	SSLinkedList<int> obj;
	char ch;
        do{
		std::cout<<"1.addNode"<<std::endl;
		std::cout<<"2.addFirst"<<std::endl;
		std::cout<<"3.addLast"<<std::endl;
		std::cout<<"4.addAtPos"<<std::endl;
		std::cout<<"5.deleteFirst"<<std::endl;
		std::cout<<"6.deleteLast"<<std::endl;
		std::cout<<"7.deleteAtPos"<<std::endl;
		std::cout<<"8.printSLL"<<std::endl;

                int choice;
		std::cout<< "enter your choice" <<std::endl;
		std::cin>> choice;

                switch(choice){
                        case 1:
                                obj.addNode();
                                break;
                        case 2:
                                obj.addFirst();
                                break;
                        case 3:
                                obj.addLast();
                                break;
                        case 4:
                                {
                                        int pos;
					std::cout<< "enter the node position" <<std::endl;
					std::cin >> pos;
                                        int ret=obj.addAtPos(pos);
                                        if(ret==-1){
						std::cout<< "invalid position" <<std::endl;
                                        }
                                }
				break;
                        case 5:{
                                       int ret=obj.deleteFirst();
                                       if(ret==-1){
					       std::cout<< "empty linked list" <<std::endl;
                                       }
                               }
                               break;
                        case 6:
                               {
                                       int ret=obj.deleteLast();
                                       if(ret==-1){
					       std::cout<< "empty linked list" <<std::endl;
                                       }
                               }
                               break;
                        case 7:
                               {
                                       int pos;
				       std::cout<< "enter the position of node" <<std::endl;
				       std::cin>> pos;
                                       int ret=obj.deleteAtPos(pos);
				       if(ret==-1){
                                               printf("invalid position\n");
                                       }
                               }
                               break;
                         case 8:
                               {
                                       int ret=obj.printSLL();
                                       if(ret==-1){
					       std::cout<<"empty LL"<<std::endl;
                                       }
                               }
                               break;
                         default:
                               std::cout<< "invalid choice" <<std::endl;
                }
                getchar();
		std::cout<< "Do you want to continue"<<std::endl;
		std::cin>>ch;
        }while(ch=='y' || ch=='Y');
	return 0;
}







