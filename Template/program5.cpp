#include<iostream>
template<typename T>
struct Node{
	T data;
	Node *prev;
	Node *next;
};
template<class T>
class DCLinkedList{
	Node <T> *head=NULL;

	public:
	int countNode(){
		Node<T> *temp=head;
		int count=0;
		while(temp->next != head){
			count++;
			temp=temp->next;
		}
		return count;
	}

	Node <T> *createNode(){
		Node <T> *newnode=(Node<T>*)malloc(sizeof(Node<T>));

		newnode->prev=NULL;

		std::cout<<"Enter the data"<<std::endl;
		std::cin>>newnode->data;

		newnode->next=NULL;
		return newnode;
	}
	void addNode(){
		Node<T> *newnode=createNode();
	      
	      	if(head==NULL){
        	        head=newnode;
                	newnode->prev=head;
                	newnode->next=head;
        	}else{
                	newnode->next=head;
                	head->prev->next=newnode;
                	newnode->prev=head->prev;
                	head->prev=newnode;
		}

	}
	void addFirst(){
		Node<T> *newnode=createNode();

		if(head==NULL){
	                head=newnode;
        	        newnode->prev=head;
                	newnode->next=head;
        	}else{
                	newnode->next=head;
                	newnode->prev=head->prev;
                	head->prev->next=newnode;
                	head->prev=newnode;
                	head=newnode;
        	}

	}
	void addLast(){
		addNode();
	}
	int addAtPos(int pos){
		int count=countNode();
		if(pos<=0 || pos>count+1){
			return -1;
		}else{
			Node<T> *newnode=createNode();
			if(pos==1){
				addFirst();
			}else if(pos==count+1){
				addLast();
			}else{
				Node<T> *temp=head;
				while(pos-2){
					temp=temp->next;
					pos--;
				}
		                newnode->next=temp->next;
        	                newnode->prev=temp;
	                        temp->next=newnode;
                	        newnode->next->prev=newnode;

			}
		}
	}

	int deleteFirst(){
		if(head==NULL){
			return -1;
		}else if(head->next==head){
			free(head);
			head=NULL;
		}else{
			Node<T> *temp=head;
                        while(temp->next != head){
                                temp=temp->next;
                        }
			head->next->prev=head->prev;
                        head=head->next;
                        free(head->prev->next);
                        head->prev->next=head;

		}
	}
	int deleteLast(){
		if(head==NULL){
			return -1;
		}else if(head->next==head){
			free(head);
			head=NULL;
		}else{
			Node<T> *temp=head;
			while(temp->next->next != head){
				temp=temp->next;
			}
		
			head->prev=head->prev->prev;
                        free(head->prev->next);
                        head->prev->next=head;

		}
	}
	int deleteAtPos(int pos){
		int count=countNode();
		if(pos<=0 || pos>count+1){
			return -1;
		}else{
			if(pos==1){
				deleteFirst();
			}else if(pos==count+1){
				deleteLast();
			}else{
				Node<T> *temp=head;
				while(pos-2){
					temp=temp->next;
					pos--;
				}
				temp->next=temp->next->next;
				free(temp->next->prev);
                        	temp->next->prev=temp;

			}
		}
	}
	int printSLL(){
		if(head==NULL){
			return -1;
		}else{
			Node<T> *temp=head;
			while(temp->next != head){
				std::cout<< temp->data <<std::endl;
				temp=temp->next;
			}
			std::cout<< temp->data <<std::endl;
		}
	}
};
int main(){
	DCLinkedList<int> obj;
	char ch;
        do{
		std::cout<<"1.addNode"<<std::endl;
		std::cout<<"2.addFirst"<<std::endl;
		std::cout<<"3.addLast"<<std::endl;
		std::cout<<"4.addAtPos"<<std::endl;
		std::cout<<"5.deleteFirst"<<std::endl;
		std::cout<<"6.deleteLast"<<std::endl;
		std::cout<<"7.deleteAtPos"<<std::endl;
		std::cout<<"8.printSLL"<<std::endl;

                int choice;
		std::cout<< "enter your choice" <<std::endl;
		std::cin>> choice;

                switch(choice){
                        case 1:
                                obj.addNode();
                                break;
                        case 2:
                                obj.addFirst();
                                break;
                        case 3:
                                obj.addLast();
                                break;
                        case 4:
                                {
                                        int pos;
					std::cout<< "enter the node position" <<std::endl;
					std::cin >> pos;
                                        int ret=obj.addAtPos(pos);
                                        if(ret==-1){
						std::cout<< "invalid position" <<std::endl;
                                        }
                                }
				break;
                        case 5:{
                                       int ret=obj.deleteFirst();
                                       if(ret==-1){
					       std::cout<< "empty linked list" <<std::endl;
                                       }
                               }
                               break;
                        case 6:
                               {
                                       int ret=obj.deleteLast();
                                       if(ret==-1){
					       std::cout<< "empty linked list" <<std::endl;
                                       }
                               }
                               break;
                        case 7:
                               {
                                       int pos;
				       std::cout<< "enter the position of node" <<std::endl;
				       std::cin>> pos;
                                       int ret=obj.deleteAtPos(pos);
				       if(ret==-1){
                                               printf("invalid position\n");
                                       }
                               }
                               break;
                         case 8:
                               {
                                       int ret=obj.printSLL();
                                       if(ret==-1){
					       std::cout<<"empty LL"<<std::endl;
                                       }
                               }
                               break;
                         default:
                               std::cout<< "invalid choice" <<std::endl;
                }
                getchar();
		std::cout<< "Do you want to continue"<<std::endl;
		std::cin>>ch;
        }while(ch=='y' || ch=='Y');
	return 0;
}







