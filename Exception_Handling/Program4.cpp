//Exception Handling code 
//throw the class approach

#include<iostream>

class InvalidIndex{
	std::string str;
	public:
		InvalidIndex(std::string str){
			this->str = str;
		}

		std::string getData(){
			return str;
		}
};
class Demo{
	
	int arr[5]={10,20,30,40,50};

	public:
	int operator[](int index){
		if(index < 0 || index >= arrLength()){
			throw InvalidIndex("Bad Index");
		}
		return arr[index];
	}
	int arrLength(){
		int size = sizeof(arr)/sizeof(arr[0]);
		return size;
	}
};
int main(){
	Demo obj;

	try{
		std::cout<< obj[-3] << std::endl;
	}catch(InvalidIndex obj){
		std::cout<< "Exception Occured : " << obj.getData() <<std::endl;
	}
}
