//class madhil serva gosti ya bydefault private asstat

#include<iostream>
class Demo{
	int x=10;

	public:
	void fun(){
		std::cout<< x << std::endl;//10
	}
};
int main(){
//	std::cout<< x << std::endl;//error

	Demo obj;
	
//	std::cout<< obj.x << std::endl;//error

	obj.fun();
	return 0;
}

/*
 * output:
 *  In function ‘int main()’:
p25.cpp:11:14: error: ‘x’ was not declared in this scope
   11 |  std::cout<< x << std::endl;//error
      |              ^
p25.cpp:15:18: error: ‘int Demo::x’ is private within this context
   15 |  std::cout<< obj.x << std::endl;//error
      |                  ^
p25.cpp:3:6: note: declared private here
    3 |  int x=10;
      |      ^
*/
