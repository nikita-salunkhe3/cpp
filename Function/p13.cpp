//pointer constant 

#include<iostream>
int main(){

	int x=10;

	int *const ptr=&x;

	int y=20;

	ptr=&y;
}
/*
 * output:
 *  error: assignment of read-only variable ‘ptr’
   10 |  ptr=&y;
      |  ~~~^~~

 */
