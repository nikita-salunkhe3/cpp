/*
 * CPP madhe y(20) ass initialization kel teri chalt 
 * z{30}--->>> List Initialization/List Initialization
 */

#include<iostream>

int main(){
	int x=10;//Value Initialization

	int y(20);//Direct Initialization

	std::cout<< x <<std::endl;//10
	std::cout<< y <<std::endl;//20

	int z{30};//Uniform Initialization / List Initialization

	std::cout<< z <<std::endl;//30
}
/*
 * output:
 * 10
 * 20
 * 30
 */


