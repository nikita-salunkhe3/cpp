#include<iostream>
int main(){
	int x=10;
	int z=100;

         int *const ptr =&z;

	std::cout<< *ptr << std::endl;
	std::cout<< &x << std::endl;
	std::cout<< ptr << std::endl;

	int y=20;

	ptr=&y;
	std::cout<< *ptr << std::endl;
	std::cout<< &y << std::endl;
	std::cout<< ptr << std::endl;
}
/*
 * output:
 * error :error: assignment of read-only variable ‘ptr’
   14 |  ptr=&y;
      |  ~~~^~~

      int *const ptr=&z;
      here ptr pointer is constant
 */ 
