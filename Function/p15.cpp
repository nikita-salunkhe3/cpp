#include<iostream>
int main(){

	int x=10;
	const int *const ptr=&x;//No error

	int y=20;
	ptr=&y;//error
}
/*
 * output:
 * error: assignment of read-only variable ‘ptr’
    8 |  ptr=&y;
      |  ~~~^~~
*/
