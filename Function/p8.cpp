/*
 Function-->> Constant

*/

#include<iostream>
int main(){
	int x=10;
	int y=20;

	float a=10.5f;
	std::cout<< x <<std::endl;//10
	std::cout<< y <<std::endl;//20

	const int *ptr=&x;
	std::cout<< *ptr <<std::endl;//10

	ptr=&a;//error
	std::cout<< *ptr << std:: endl;//20

	int const *ptr1=&x;
	std::cout<< *ptr1 <<std::endl;

	ptr1=&a;//error
	std::cout<< *ptr1 << std:: endl;//20


	return 0;
}
/*
 * output:
 * error: cannot convert ‘float*’ to ‘const int*’ in assignment
   18 |  ptr=&a;
      |      ^~
      |      |
      |      float*
p8.cpp:24:7: error: cannot convert ‘float*’ to ‘const int*’ in assignment
   24 |  ptr1=&a;
      |       ^~
      |       |
      |       float*
*/

