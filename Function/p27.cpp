#include<iostream>
int* fun(int* val){
	int a=*val + 40;

	std::cout<< *val <<std::endl;//10
	std::cout<< a <<std::endl;//50
	return &a;
}
int main(){
	int x=10;
	std::cout<< x <<std::endl;//10

	int *ret=fun(&x);
	std::cout<< ret <<std::endl;//0/GV
	std::cout<< *ret <<std::endl;//Segmentation fault
	return 0;
}
/*
 * output:
 * warning: address of local variable ‘a’ returned [-Wreturn-local-addr]
    7 |  return &a;
      |         ^~
*/



