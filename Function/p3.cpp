//Variable Shadowing
/*
 * one variable hides to an another variable
 * Two Type of variable Shadowing
 * 1)Local Vairable Shadowing
 * 2)Gobal Vairable Shadowing
 *
 * if In code Same variable are present then this concept can be used
 */

#include<iostream>
int main(){
	int x=10;
	std::cout<< x <<std::endl;//10

	{
		int x=20;
		std::cout<< x <<std::endl;//20
		x=30;
		std::cout<< x <<std::endl;//30
	}
	std::cout<< x <<std::endl;//10

	return 0;
}
/*
 * output:
 * 10
 * 20
 * 30
 * 10
 *
 * Explanation:jeva same name cha variable apan lihu teva to print kertana javal cha variable chi
 * value print kerto
 */
