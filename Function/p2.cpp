/*
 * if we write as a{} uniform initialization check that dataype is compatible or Not.
 * if not then it will gives an error
 */
#include<iostream>

int main(){
	int x=10;
	int y=20.5f;//ya secnario madhe tyane check nahi kel compatible ahe ki nahi 

	std::cout<< x <<std::endl;
	std::cout<< y <<std::endl;

	int a{10};
//	int b{10.50f};//error-->>check kerto chotya box madhe mothi value thevli ahe 

	float p{10.11f};
	std::cout<< p <<std::endl;
}
/*
 * output:
 * error: narrowing conversion of ‘1.05e+1f’ from ‘float’ to ‘int’ [-Wnarrowing]
   11 |  int b{10.50f};

 */
/*
 * output:
 * 10
 * 20
 * 10.11
 */
