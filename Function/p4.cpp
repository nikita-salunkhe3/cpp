/*Variable Shadowing
  (::) scope resolution operator ha fkt Global gosti access kernya sathi used kela jato
  scope resolution ha global gosti gheun yenya sathi used kertat 
  *Local Variable shadowing madhe scope resolution operator used kerta yet nahi
  *Global Variable shadowing madhe scope resolution operator used kela jato
 */

#include<iostream>
int main(){
	int x=10;
	std::cout<< x <<std::endl;

	{
		int x=20;
	   	std::cout<< x <<std::endl;
	   	std::cout<< ::x <<std::endl;//error-->>Invalid used of Scope resolution operator

		x=30;
		std::cout<< x <<std::endl;
	}
	std::cout<< x <<std::endl;
	return 0;
}
/*
 * output:
 * error: ‘::x’ has not been declared
   12 |      std::cout<< ::x <<std::endl;

Explanation:this error can be given by Local Variable Shadowing

 */
