/*
 Reference Variable
 Reference la jaga nahi lagat

 Internally reference ha pointer menun ch jato ...compiler internally &y==*y kerto ani x=&x ass ch kerto
 ya madhe y ha pointer zala ya pointer cha box madhe (&x) x cha address asel ani jeva apan
 'y' print kerayala jau teva complier swata *y(Value at) ass kerto menjech dereference kerto
 menun aplyala x cha box madhli value bhetet
 */

#include<iostream>
int main(){
	int x=10;
	int &y=x;//internal call -->> int *y=&x

	std::cout<< &x <<std::endl;//0x100
	std::cout<< &y <<std::endl;//0x100

	std::cout<< x <<std::endl;//10
	std::cout<< y <<std::endl;//10
	return 0;

}
/*
 * output:
 * 0x100
 * 0x100
 * 10
 * 10
 */

