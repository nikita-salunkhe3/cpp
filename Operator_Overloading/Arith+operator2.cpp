//Overload Arithmetic + operator using Member function
#include<iostream>
class Demo{
	int x=10;
	public:

	Demo(){
		std::cout<< "In Constructor "<<std::endl;
	}
	int operator+(const Demo& obj1){

		return this->x + obj1.x;
	}
};
int main(){
	std::cout<< "In main" <<std::endl;
	Demo obj1;
	Demo obj2;

	std::cout<< obj1+obj2 <<std::endl;
	return 0;
}
