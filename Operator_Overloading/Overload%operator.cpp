//overload operator% using Member function

#include<iostream>
class Demo{
	int x=10;
	int y=20;

	public:
	Demo(){
		std::cout<< "In Constructor" <<std::endl;
	}
	int operator%(const Demo& obj1){
		std::cout<< "Member " <<std::endl;
		return this->x % obj1.y;
	}
};
int main(){
	Demo obj1;
	Demo obj2;

	std::cout<< obj1%obj2 <<std::endl;
	return 0;
}


