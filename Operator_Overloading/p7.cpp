#include<iostream>
	
class Demo{
	int x=10;
	int y=20;

	public:
	Demo(int x,int y){
		this->x=x;
		this->y=y;
	}

	int getDataX()const{
		return x;
	}
	int getDataY()const{
		return y;
	}
};
int operator<(const Demo& obj1,const Demo& obj2){

	if(obj1.getDataX() < obj2.getDataX() && obj1.getDataY() < obj2.getDataY()){
		return 1;
	}else{
		return 0;
	}
}
int main(){
	Demo obj1(30,40);
	Demo obj2(50,60);

	std::cout<< (obj1 < obj2) <<std::endl;
	return 0;
}
