//Overload operator- using Normal function but write friend function as well as member function in it

#include<iostream>
class Demo{
	int x=10;
	public:

	int operator-(const Demo& obj){
		std::cout<< "Member" <<std::endl;
		return this->x - obj.x;
	}
	friend int operator-(const Demo& obj1, const Demo& obj2){
		std::cout<< "Friend" <<std::endl;
		return obj1.x + obj2.x;
	}
	int getData(){
		return x;
	}
};
int operator-(Demo& obj1,Demo& obj2){
	std::cout<< "Normal" <<std::endl;
	return obj1.getData() + obj2.getData();
}
int main(){
	Demo obj1;
	Demo obj2;

	std::cout<< obj1-obj2 <<std::endl;
	return 0;
}
