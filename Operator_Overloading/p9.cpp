//Overload the operator by using friend function
//we should write friend function in class scope or as Normal function there is no change int contain

#include<iostream>
class Demo{	
	public:
		int x=10;

		Demo(){

			std::cout<< "In constructor" <<std::endl;
			}
		friend std::ostream& operator<<(const std::ostream& out, Demo& obj){
			std::cout<<"In frined"<<std::endl;
		//	out<<obj.x;
			return out;//if we not write return statement then it will only gives warning 
			//because here call by address concept is going on
		}	
		Demo& getData(const Demo& obj){
			std::cout<< x <<std::endl;
			return obj;
		}
};
int main(){
	Demo obj;

	Demo obj1;
	std::cout<< obj <<std::endl;
	obj.getData(obj1);
	return 0;
}
//error
//const std::ostream& obj===> normal function lihitana ostream cha reference nahi lihaycha error yeto
//karen 
/*
 * explaination :
 * The error message "binding reference of type 'Demo&' to 'const Demo' discards qualifiers" means that you are trying to bind a non-const reference to a const object. This is not allowed in C++, because it would allow you to modify the object through the reference, even though the object is const.
 * */
