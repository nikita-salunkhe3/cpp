#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x=x;
	}
/*	friend int operator+(const Demo& obj1, const Demo& obj2){
		std::cout<< "In Friend Function" <<std::endl;
		return obj1.x + obj2.x;
	}*/
	int operator+(const Demo& obj){
		std::cout<< "In Member function " <<std::endl;
		return this->x + obj.x;
	}
	int getData()const {
		return x;
	}
};
/*
int operator+(const Demo& obj1,const Demo& obj2){
	std::cout<< "Normal "<<std::endl;
	return obj1.getData() + obj2.getData();
}*/
int main(){
	Demo obj1(20);
	Demo obj2(30);

	std::cout<< obj1+obj2 <<std::endl;
	std::cout<< obj1+80 <<std::endl;
	std::cout<< 80+obj2 <<std::endl;
	return 0;
}


