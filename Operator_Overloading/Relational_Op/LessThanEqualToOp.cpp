/*
 * ***********Overload <= Operator***********/
/*************Overload >= Operator ***********/
/*************Overload == Operator ***********/

/**All Three Operators are overloaded using Friend, member and Normal function**/

#include<iostream>
class Demo{
	int x=10;
	int y=10;

	public:
	Demo(){
		std::cout<< "Constructor" <<std::endl;
	}
	bool operator<=(const Demo& obj2){
		std::cout<< "Member "<<std::endl;
		return this->x <= obj2.y;
	}
/*	friend bool operator<=(const Demo& obj1,const Demo& obj2){
		std::cout<<"Friend"<<std::endl;
		return obj1.x <= obj2.y;
	}*/
	friend bool operator==(Demo& obj1, Demo& obj2){
		std::cout<<"In Friend "<<std::endl;
		return obj1.x == obj2.y;
	}
	int getDataX()const{
		return x;
	}
	int getDataY()const{
		return y;
	}
};
bool operator<=(const Demo& obj1,const Demo& obj2){
	std::cout<< "Normal" <<std::endl;
	return obj1.getDataX() <= obj2.getDataY();
}
bool operator>=(const Demo& obj1, const Demo& obj2){
	std::cout<< "Normal" <<std::endl;
	return obj1.getDataX() >= obj2.getDataY();
}
/*
bool operator==(const Demo& obj1,const Demo& obj2){
	std::cout<< "Normal" <<std::endl;
	return obj1.getDataX() == obj2.getDataY();
}*/
int main(){
	Demo obj1;
	Demo obj2;

	std::cout<< (obj1<=obj2)<< std::endl;
	std::cout<< (obj1>=obj2)<< std::endl;
	std::cout<< (obj1==obj2)<< std::endl;
	return 0;
}


