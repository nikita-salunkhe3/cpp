/************Overload != Operator**********/

//1. Using Friend Function
//2. Using Member Function
//3. Using Normal Function


#include<iostream>
class Demo{
	int x=10;

	public:
	Demo(int x){
		this->x =x;
		std::cout<<"Constructor"<<std::endl;
	}
/*	bool operator!=(const Demo& obj2){
		std::cout<< "In Member "<<std::endl;
		return this->x != obj2.x;
	}*/
/*	friend bool operator!=(const Demo& obj1, const Demo& obj2){
		std::cout<< "In Friend" <<std::endl;
		return obj1.x != obj2.x;
	}*/
	int getData()const{
		return x;
	}
};
bool operator != (const Demo& obj1,const Demo& obj2){
	std::cout<< "In Normal"<<std::endl;
	return obj1.getData() != obj2.getData();
}
int main(){
	Demo obj1(10);
	Demo obj2(30);

	std::cout<< (obj1 != obj2) <<std::endl;
	return 0;
}

