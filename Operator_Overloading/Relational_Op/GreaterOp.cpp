/**Overload Operator Greator Than (<) *************/
//1. Using Friend Function
//2. Using Normal Function
//3. Using Member function

/*
 * jeva jeva apan Relational Operator overload karat asu teva Bracket Nakki dyacha karen << operator la <
 * greator that peksha jast priority ahe tyamule cout Obj1 la Khechun gheto yamule (obj1 < obj2) ass 
 * lihaycha yamule () brackets chi Priority high hote
 */

#include<iostream>
class Demo{
	int x=10;

	public:
	Demo(int x){
		this->x = x;
		std::cout<< "In constructor" <<std::endl;
	}
/*	friend bool operator>(const Demo& obj1, const Demo& obj2){
		return obj1.x > obj2.x;
	}*/
	int getData()const{
		return x;
	}
	bool operator>( const Demo& obj2){
		return this->x > obj2.x;
	}
};
bool operator>(const Demo& obj1,const Demo& obj2){
	return obj1.getData() > obj2.getData();
}
int main(){
	Demo obj1(40);
	Demo obj2(30);

	std::cout<< (obj1 > obj2) <<std::endl;
	return 0;
}
