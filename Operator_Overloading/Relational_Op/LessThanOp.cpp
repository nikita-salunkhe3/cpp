/********Overload < Less Than Operator ***************/
//1. Using Friend Function
//2. Using Normal Function
//3. Using Member function

#include<iostream>
class Demo{
	int x;

	public:
	Demo(int x){
		this->x = x;
		std::cout<< "Constructor" <<std::endl;
	}
	friend bool operator<(const Demo& obj1, const Demo& obj2){
		std::cout<< "In Friend " <<std::endl;
		return obj1.x < obj2.x;
	}
	bool operator<(const Demo& obj2){
		std::cout<< "In Member"<<std::endl;
		return this->x < obj2.x;	
	}
};
int main(){
	Demo obj1(10);
	Demo obj2(20);

	std::cout<< (obj1< obj2) <<std::endl;
	return 0;
}


