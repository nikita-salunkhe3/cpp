/*
 * overload Arithmetic + operator using Friend function
 */

#include<iostream>
class Demo{
	int x=10;

	public:
	Demo(){
		std::cout<< "Constructor" <<std::endl;
	}
	friend int operator+(const Demo& obj1,const Demo& obj2){
		return obj1.x+obj2.x;
	}
};
int main(){
	Demo obj1;
	Demo obj2;

	std::cout<< obj1+obj2 <<std::endl;
	return 0;
}



