#include<iostream>
class Demo{
	int x=10;
	int y=20;

	public:
	Demo(int x){
		this->x=x;
	}
	int operator+(const Demo& obj){
		std::cout<< "In Member" <<std::endl; 
		return this->x + obj.x;
	}
	friend int operator+(const Demo& obj1, const Demo& obj2){
		std::cout<< "In Friend" <<std::endl; 
		return obj1.x + obj2.x;
	}
};
int main(){
	Demo obj1(10);
	Demo obj2(30);

	std::cout<< obj1+obj2 <<std::endl;
	std::cout<< obj1+100 <<std::endl;
	std::cout<< 100+obj2 <<std::endl;
	return 0;
}
