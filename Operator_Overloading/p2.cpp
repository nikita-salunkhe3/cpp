/*
 * OPERATOR OVERLOADING
 * operator sathi function lihitoy menun operator overloading
 */
#include<iostream>
class Demo{
	int x=10;
	int y=20;
	public:
	
	Demo(int x,int y){
		this->x=x;
		this->y=y;
	}
	friend int operator/(const Demo& obj1,const Demo& obj2){
		std::cout<<"Friend"<<std::endl;
		return obj1.x/obj2.x;
	}
};
int main(){
	Demo obj1(100,200);
	Demo obj2(30,40);

	std::cout<< obj1/obj2 <<std::endl;
	return 0;
}

