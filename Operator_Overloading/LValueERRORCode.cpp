#include<iostream>
class Demo{
	int x=10;
	public:
	Demo(int x){
		this->x = x;
	}
	int operator+( Demo obj){
	//	obj.x=5;//if given function parameter is constant error - readOnly access because our object is constant 
		std::cout<< "Member "<<std::endl;
		return this->x + obj.x;
	}
	Demo(Demo& obj){
		std::cout<< "Copy Constructor " <<std::endl;
	}
};
int main(){
	Demo obj1(20);
	Demo obj2(30);

	std::cout<< obj1+obj2 <<std::endl;//call to copy constructor
	std::cout<< obj1+100 <<std::endl;/*error--cannot bind non-const lvalue reference of type ‘Demo&’ to an rvalue of type ‘Demo’
   22 |  std::cout<< obj1+100 <<std::endl;//error--
      |                   ^~~
*/
	return 0;
}



