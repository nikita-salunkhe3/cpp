//If there is no error to new Operator then why we overload operator new 
//Because by overloading new operator we can take extra paramter to operator new() function

#include<iostream>

class Demo{

	public:

	Demo(){
		std::cout<< "Demo Constructor"<<std::endl;
	}
	void* operator new(size_t size){

		std::cout<<"In new Operator" <<std::endl;

		void *ptr=malloc(size);
		return ptr;
	}
	~Demo(){
		std::cout<< "In Demo Destructor" <<std::endl;
	}
};

int main(){
	Demo *obj = new Demo();
	delete obj;
	return 0;
}

