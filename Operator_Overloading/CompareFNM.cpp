#include<iostream>
class Demo{
	int x=10;

	public:
	Demo(){
		this->x = x;
	}
	friend int operator+(const Demo& obj1, const Demo& obj2){
	
		std::cout<< "In Friend function" <<std::endl;
		return obj1.x + obj2.x;
	}
	int operator+(const Demo& obj){
		std::cout<< "In Member function" <<std::endl;
		return x+obj.x;
	}
	int getData()const{
		return x;
	}
};

int operator+(Demo& obj1, Demo& obj2){
	return obj1.getData() + obj2.getData();
}
int main(){
	Demo obj1;
	Demo obj2;

	std::cout<< obj1+obj2 <<std::endl;
	return 0;
}

