/****Why we do Operator overloading*******/
/*
 * CPP does not gives functoin for our class because cpp does not know that what type of class we are 
 * written for that purpose we are using operator overloading
 * In cpp all operator can internaly call as a function and then it can be goes to CPU register
 * As Like Function Overloading cpp can overload operator so that it can be name as operator overloading
 */

#include<iostream>
class Demo{
	int x=10;

	public:
	std::cout<< "In constructor" <<std::endl;
};
int main(){
	Demo obj;
	std::cout<< obj << std::endl;//error because its internal call is like
	//std::ostream& operator<<(const std::ostream& , const Demo& )
//CPP does not have predefined function as like this
}
