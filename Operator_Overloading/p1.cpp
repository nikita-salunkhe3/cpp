//Operator Overloading:
//Internally Implicitly call dila jato tyala Operator Overloading ase mentyat

/*
 * Q. Operator Overloading picture madhe ka aala?
 * -->>   std::cout<< obj <<std::endl;
 *       
 *       jer hi line compile keli ter error yeto karen internally ha call
 *       ostream& operator<<(ostream& cout, Demo& obj);
 *       ya madhe operator<< la samjat nahi tyala ky print keraych ahe meunu to error deto
 *       aplya class cha navach pridefine function operator<< sathi lihilel nahiye
 *       tyamule tyala match keranre ek hi function bhetat nahi tyamule error ali
 *
 *       ani ya operator<< sathi che function aplyala lihita yave menun operator Overloading chi
 *       concept used keli jate
 */
/*
 * Example:
 *  x + y  ha interanlly apan lihilelya method implicity call krel (operator+) 
 *  tya nanter apan lihilel function return integer value krel 
 *  mg ya interger value la match karun internally operator<< la predefine method la call jail
 *
 *  CPP madhe asdhi internally apan lihilelya operator+ kade call jail ani mg nanter CPU kade run hoyla 
 *  jail. x + y internally CPU kade instruction run honya sathi jail tyatil x ani y chi value register
 *  madhe store hoil ani ALU(arithmatic and Logical unit) yacha madatine x+y he addition ch 
 *  operation perform hoil.
 *
 *  C madhe x+y he operation honya sathi direct he instruction CPU Run hoyla jail
 */

#include<iostream>
class Demo{
	int a=10;
	public:
	Demo(){

	}
};
int main(){
	Demo obj;
	int x=10;
	std::cout<< x <<std::endl;
	std::cout<< obj <<std::endl;//print nahi hot ya sathi operator Overloading chi concept aali ahe

	return 0;
}
/*
 error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Demo’)
   43 |  std::cout<< obj <<std::endl;
      |  ~~~~~~~~~^~ ~~~
      |       |      |
      |       |      Demo
      |       std::ostream {aka std::basic_ostream<char>}

 */


