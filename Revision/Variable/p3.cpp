#include<iostream>
int main(){
	int x='A';

	std::cout<< x <<std::endl;//65

	int y('A');

	std::cout<< y <<std::endl;//65
	
	int z{'A'};//here Uniform Initialization can strictly check datatype but in this secnario 
	//it convert "A" character in ASCII value so there is no error

	std::cout<< z <<std::endl;//65

	return 0;
}
