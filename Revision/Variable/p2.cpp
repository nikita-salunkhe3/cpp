#include<iostream>
int main(){
	int x=10.5f;

	std::cout<< x <<std::endl;//10

	int y(10.5f);

	std::cout<< x <<std::endl;//10
	
//	int z{10.5f};//Uniform Initialization can strictly check the datatype

//	std::cout<< x <<std::endl;//error
	
	return 0;
}
/*output:
 * error: narrowing conversion of ‘1.05e+1f’ from ‘float’ to ‘int’ [-Wnarrowing]
   11 |  int z{10.5f};
      |             ^
*/
