#include<iostream>
class Demo{
	int x=10;
	int y=20;

	public:
	Demo(){
		std::cout<< "In No-args" <<std::endl;
	}
	Demo(int x,int y){
		this->x=x;
		this->y=y;
		std::cout<< "In Para" <<std::endl;
	}
	Demo(Demo &ref){
		std::cout<< "Copy Constructor" <<std::endl;
	}
	Demo& info(Demo &obj){//reference pass kela ahe menun navin gola tayar nahi zala to actual madhe obj1 madhil 10 hi value pusun 700 value takli.....
		obj.x=700;
		obj.y=800;
		return obj;
	}
	void access(){
		std::cout<< x <<"  "<< y << std::endl;
	}
};
int main(){
	Demo obj1;
	obj1.access();
	Demo obj2(100,200);
	Demo ret=obj2.info(obj1);
	ret.access();//class chi original copy bhete
	obj1.access();//value change zali golyatil
	return 0;
}


