
#include<iostream>
class Demo{
	int x=10;
	int y=20;

	public:
	Demo(){
		std::cout<< "In No-args" <<std::endl;
		std::cout<< x << "  " << y <<std::endl;
	}
	Demo(int x,int y){
		std::cout<< "Para" <<std::endl;
		this->x=x;
		this->y=y;
		std::cout<< this->x << "  " << y <<std::endl;
	}
	Demo(Demo &obj){
		Demo();
		std::cout<< "Copy" <<std::endl;
		std::cout<< x << "  " << y <<std::endl;
	}
	void fun(){
		std::cout<< x << "  " << y <<std::endl;
	}
		
};
int main(){
	Demo obj1;
	Demo obj2(100,200);
	Demo obj3(obj1);
	obj3.fun();//10  20
	obj1.fun();//10  20
	obj2.fun();//100  200
	return 0;
}
/*
 * output:
 * In No-args
 * 10  20
 * Para
 * 100  200
 * In No-args
 * 10 20
 * Copy
 * 10  20
 */

