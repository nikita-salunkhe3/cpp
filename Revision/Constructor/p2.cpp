#include<iostream>
class Demo{
	int x=10;
	int y=20;

	public:
	Demo(){
		std::cout<< "No-args" <<std::endl;
		std::cout<< x << "  " << y <<std::endl;//obj2 madhil x,y variable print hoil
	}
	Demo(int x, int y){

		std::cout<< "Para" <<std::endl;
		this->x = x;
		this->y = y;
	}
	Demo(Demo &obj){
		std::cout<< "Copy" <<std::endl;
	}
	void info(Demo obj){
		std::cout<< "In info" <<std::endl;
		std::cout<< x << "  " << y <<std::endl;//obj2 madhil x,y variable print hoil
		std::cout<< obj.x << "  " << obj.y <<std::endl;
	  	//obj1 madhil x,y variable print hotil

		obj.x=1000;
		obj.y=2000;
	}
	void fun(Demo obj){
		std::cout<<"In fun" <<std::endl;
		std::cout<< obj.x << "******" << obj.y <<std::endl;
	}
};
int main(){
	Demo obj1;
	Demo obj2(100,200);
	obj2.info(obj1);
	obj2.fun(obj1);
	return 0;
}
/*output:
 * No-args
 * para
 * Copy
 * In info
 * 100  200
 * 10  20
 */

