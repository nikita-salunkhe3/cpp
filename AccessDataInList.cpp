#include<iostream>
#include<list>

std::_List_iterator<int> operator+(std::_List_iterator<int> obj,int index){

	while(index){
		obj++;
		index--;
	}
	return obj;
}
int main(){

	std::list<int> lst={10,20,30,10,20,40};

	std::list<int>::iterator itr;

//	itr=lst.begin();
	std::cout<< *((lst.begin())+2) <<std::endl;//30
}

/*
 * Output:30
 */

/*
 *  error: cannot declare reference to ‘struct std::_List_iterator<int>&’, which is not a typedef or a template type argument
    4 | std::_List_iterator<int>& operator+(std::_List_iterator<int>& &obj,int index){
      |
 */

