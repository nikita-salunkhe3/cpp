//Code Snippets
//Imp code

#include<iostream>
class Parent{
	int x=10;
	int y=20;

	public:
	Parent(){
		std::cout<< "Parent No-args Constructor" <<std::endl;
		std::cout<< "Parent No-args this = " << this <<std::endl;//0x200
	}
	Parent(int x,int y){
		
		this-> x =x;
		this-> y = y;
		std::cout<< "Parent Para Constructor" <<std::endl;
		std::cout<< "Parent Para this " << this <<std::endl;//0x300

	}
	void printData(){
		std::cout<< x << "  " << y <<std::endl;
	}
	~Parent(){
		std::cout<< "Parent Destructor" <<std::endl;
	}
};
class Child:public Parent{
	int z=30;

	public:
	Child(int x,int y,int z){
		//bydefault automatically internally Parent cha default() constructor lach call jato

		Parent(x,y);//new object tayar hotoy jyacha scope child cha constructor cha scope purta
		//limited ahe
		std::cout<< "Child No-args constructor"<<std::endl;
		std::cout<< "Child No-args this = "<< this <<std::endl;//0x200
	}
	void getData(){
		std::cout<< z <<std::endl;
	}
	~Child(){
		std::cout<<"Child Destructor"<<std::endl;
	}
};
int main(){

	Child obj(40,50,60);
	obj.getData();
	obj.printData();
	return 0;
}


