#include<iostream>

class Parent{
	int x =10;

	protected:
	int y = 20;

	public:
	int z=30;

	void getData(){
		std::cout << x << y << z <<std::endl;
	}
};
class Child : protected Parent{
	
	public:
	void Info(){
		std::cout<< y <<std::endl;
		std::cout<< z <<std::endl;
	}
};
int main(){
	Child obj;
	obj.Info();
	obj.getData();//Error

	Parent obj1;
	obj1.getData();
	return 0;
}



