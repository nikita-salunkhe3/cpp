#include<iostream>
class Employee{
	std::string empName = "Kanha";
	int empId = 255;

	public:
	Employee(){
		std::cout<< "Employee Constructor" <<std::endl;
	}
	void getData(){
		std::cout<< empName << "  " << empId <<std::endl;
	}
	~Employee(){
		std::cout<< "Employee Destructor" <<std::endl;
	}
};
class Company{
	std::string cName = "verites";
	int empCount = 3000;

	Employee obj;

	public:
	Company(std::string cName,int empCount){
		std::cout<< "Company Constructor" <<std::endl;
		this->cName = cName;
		this->empCount = empCount;
	}
	void getData(){
		std::cout << cName << "  " << empCount <<std::endl;
		obj.getData();
	}
	~Company(){
		std::cout<< "Company Destructor " <<std::endl;
	}
};
int main(){
	Company obj("Pubmatic",5000);
	obj.getData();
	return 0;
}



