/*
 * Problem Statement 
 * Change the Data Of parent class 
 */

#include<iostream>

class Parent{
	int x= 10;

	protected:
	int y=20;

	public:
	int z=30;

	public:
	Parent(){

		std::cout<< "In New Constructor" <<std::endl;
	}
	Parent(int x,int y,int z){
		std::cout<< "Parent Constructor" <<std::endl;

		this->x = x;
		this->y = y;
		this->z = z;
	}

	public:
	void getData(){
		std::cout<< x << y << z <<std::endl;
	}
};
class Child : Parent{
	
	public:
		Child(int x, int y,int z){

			Parent();//eth navin object tayar hotoy.........ha Default constructor la call kerel
	
			Parent(x,y,z);
		//	isuper();	
			std::cout<< "Child Constructor" <<std::endl;
		}
		void Info(){

			getData();//eth juna jo objcet hota tyacha x y z cha data print karel
		}
};
int main(){
	Child obj(40,50,60);//Parameterized Constructor
	obj.Info();
	return 0;
}

