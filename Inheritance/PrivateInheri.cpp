#include<iostream>

class Parent{

	int x = 10;

	protected:
	int y = 20;

	public:
	int z = 30;

	void getData(){
		std::cout<< x << y << z <<std::endl;
	}
};
class Child : Parent{//Privately Inheritated--->>>
	//Parent madhil je kahi Instance Variable or methods asstil tya child class madhe Privatly Yatil
	//Apana Parent class Madhi jeri data public protected aasel teri to child class madhe alyaver
	//Private houn jato 
	//menun jer apan child class madhe Parent cha Private gosti access karu shakato pn 
	//main madhun me jer Parent chi method cl keli Child cha object ver ter Error yete
	//pn hech jer me Parent cha object varun getData() method la call kela ter error yet nahi
	
	public:
	void Info(){
	//	std::cout<< x << std::endl;//Parent class madhe x variable he Private ahe tyamule to
		//Child class madhe 'x' variable yenar nahi yamule error yeil
		//Error:- Private within this context

		std::cout<< y << std::endl;
		std::cout<< z << std::endl;
	}
};
int main(){
	Child obj;
	obj.Info();
}

