#include<iostream>
class Employee{
	std::string empName = "Kanha";
	int empId = 255;

	public:
	Employee(){
		std::cout<< "In Employee Costructor" <<std::endl;
	}
	void getData(){
		std::cout<< empName << "  " << empId <<std::endl;
	}
	void setData(std::string empName,int empId){
		this->empName=empName;
		this->empId = empId;
	}
	~Employee(){
		std::cout<< "Employee Destructor" <<std::endl;
	}
};
class Company{
	std::string cName = "verties";
	int empCount = 3000;
//	Employee obj("Rahul",300);//error

	Employee obj;

	public:
	Company(std::string cName, int empCount){
		this->cName = cName;
		this->empCount = empCount;

		std::cout<< "Company Constructor" <<std::endl;
		//Employee obj;//limited scope thats way obj.getData() la error yete

	}
	void getData(){
		std::cout<< cName << "  " << empCount <<std::endl;
		obj.getData();
		obj.setData("Rahul",300);
		std::cout<< "******************" <<std::endl;
		obj.getData();	
	}
	~Company(){
		std::cout<< "Company Destructor" <<std::endl;
	}
};
int main(){
	Company obj("PubMatic",3000);
	obj.getData();
	return 0;
}

