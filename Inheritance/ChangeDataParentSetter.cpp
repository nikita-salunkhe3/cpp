#include<iostream>
class Parent{
	int x=10;
	int y=20;

	public:
	Parent(){
		std::cout<< "In Parent constructor" <<std::endl;
	}
	void setData(int x,int y){
		this->x = x;
		this-> y = y;
	}
	void getData(){
		std::cout<< x << "  " << y << std::endl;
	}
};
class Child:public Parent{
	int z=30;
	public:
	Child(int z){
		this->z = z;
		std::cout<< "In Child Constructor" <<std::endl;
	}
	void getInfo(){
		std::cout<< "In GetInfo" <<std::endl;
		getData();
	}
};
int main(){
	Child obj(40);
	obj.getData();//10  20
	obj.setData(50,60);
	obj.getData();//50  60
	return 0;
}



