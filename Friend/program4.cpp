#include<iostream>
class Two;
class One{
	int x=10;
	protected:
       	int y=20;

	public:
	One(){
		std::cout<< "In One Constructor" << std::endl;
	}
	friend void Two::accessData(const One& obj);
};
class Two{
	public:
	Two(){
		std::cout<< "In Two constructor" <<std::endl;
	}

	void accessData(const One& obj);
};
void Two::accessData(const One& obj){
	std::cout<< obj.x <<std::endl;
	std::cout<< obj.y <<std::endl;
}
int main(){
	One obj1;
	Two obj2;

	obj2.accessData(obj1);
	return 0;
}

/*
 * error: invalid use of incomplete type ‘class Two’
   12 |  friend void Two::accessData(const One& obj);
      |                                            ^
program4.cpp:2:7: note: forward declaration of ‘class Two’
    2 | class Two;
*/

