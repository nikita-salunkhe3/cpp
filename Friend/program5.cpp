#include<iostream>
class One;
class Two{
	public:
		Two(){
			std::cout<< "In Two Constructor "<<std::endl;
		}
		void accessData(const One& obj1);
};
class One{
	int x=10;
	protected:
	int y=20;

	public:
	One(){
		std::cout<< "In One Constructor" << std::endl;
	}
        friend void Two::accessData(const One& obj1);
};
void Two::accessData(const One& obj){
	std::cout<< obj.x <<std::endl;
	std::cout<< obj.y <<std::endl;
}
int main(){
	One obj1;
	Two obj2;

	obj2.accessData(obj1);
	return 0;
}

	
